import { $, connect, useEffect, useState, useStyles, useIcon } from "tess-core";
import styles from "./styles";
import props from "./props";
import { Host } from "layout";

function uiIcon(props) {
  const [svg, setSvg] = useState("");
  const { css } = useStyles(styles, { ...props }, [
    props.w,
    props.name,
    props.color,
    props.stroke,
    props.line,
    props.rotate,
    props.dev,
  ]);

  useEffect(() => {
    useIcon(props.name).then(svg => setSvg(svg));
  }, []);

  return $(
    <Host stylesheet={css}>
      <template>{svg}</template>
    </Host>
  );
}

export default connect(uiIcon)({ props });