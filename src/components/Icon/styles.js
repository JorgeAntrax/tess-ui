import { css } from "tess-core";

const getWidth = value => {
  if (!value.length) return "";
  
  if (value.indexOf("px") !== -1 || value.indexOf("%") !== -1)
    return css`
      width: var(--ui-icon-w, ${value});
      min-width: var(--ui-icon-w, ${value});
      height: var(--ui-icon-w, ${value});
      min-height: var(--ui-icon-w, ${value});
    `;

  if (isNaN(value))
    return css`
      width: var(--ui-icon-w, 24px);
      min-width: var(--ui-icon-w, 24px);
      height: var(--ui-icon-w, 24px);
      min-height: var(--ui-icon-w, 24px);
    `;

  return css`
    width: var(--ui-icon-w, ${value * 5}%);
    min-width: var(--ui-icon-w, ${value * 5}%);
    height: var(--ui-icon-w, ${value * 5}%);
    min-height: var(--ui-icon-w, ${value * 5}%);
  `;
};

const styles = ({ w, rotate, color, stroke, line, dev }) => css`
  :host {
    display: inline-flex;
    align-items: center;
    align-content: center;
    justify-content: center;
    position: relative;
    ${w && getWidth(w)}
  }

  :host > span {
    display: inline-flex;
    align-items: center;
    align-content: center;
    justify-content: center;
    position: relative;

    width: 100%;
    height: 100%;

    transform-origin: center center;
    transition: all 200ms linear;
    ${rotate && `transform: rotate(${rotate || 0}deg);`}
  }

  :host svg {
    ${w && getWidth(w)}
  }

  :host svg [fill] {
    fill: var(--ui-icon-fill, ${color || "black"});
  }

  :host svg [stroke] {
    stroke: var(--ui-icon-color, ${color || "black"});
  }

  :host svg [stroke-width] {
    stroke-width: var(--ui-icon-stroke-width, ${stroke || 1});
  }

  :host svg [stroke-linejoin],
  :host svg [stroke-linecap] {
    stroke-linejoin: var(--ui-icon-line, ${line || "round"});
    stroke-linecap: var(--ui-icon-line, ${line || "round"});
  }

  ${dev &&
  `
			:host > span {
				box-shadow: inset 0 0 0 1px rgba(0, 0, 230, 0.5);
				border-radius: 8px;
			}
		`}

  ${dev &&
  `
			:host > span > svg {
				background-color: rgba(255,0,0,0.2); 
				box-shadow: inset 0 0 0 1px white;
				padding: 2px;
				border-radius: 8px;
				transform: scale(0.9);
			}
		`}
`;

export default styles;
