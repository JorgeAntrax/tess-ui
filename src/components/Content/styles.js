import { css, getCssProperty } from "tess-core";

const getSize = (key, value) => {
  if (!value.length) return "";
  if (value.indexOf("px") !== -1 || value.indexOf("%") !== -1) {
    return css`
      @media screen and (min-width: ${getCssProperty("media-" + key)}) {
        :host > div {
          width: 100%;
          max-width: ${value};
          margin-left: auto;
          margin-right: auto;
        }
      }
    `;
  }

  if (isNaN(value)) return "100%";
  return css`
    @media screen and (min-width: ${getCssProperty("media-" + key)}) {
      :host > div {
        width: 100%;
        max-width: ${+value * 5}%;
        margin-left: auto;
        margin-right: auto;
      }
    }
  `;
};

const styles = ({ xs, sm, md, lg, xl, wrapper, dev }) => css`
	:host {
		display: block;
	}

	${
		dev &&
		`
			:host {
				box-shadow: inset 0 0 0 1px rgba(0, 0, 230, 0.5);
				padding: 2px;
				border-radius: 4px;
			}
		`
	}

	${
		dev &&
		`
			:host > div {
				background-color: rgba(255,0,0,0.2); 
				box-shadow: inset 0 0 0 1px white;
				padding: 2px;
				border-radius: 4px;
			}
		`
	}

	${xs && getSize("xs", xs)}
	${sm && getSize("sm", sm)}
	${md && getSize("md", md)}
	${lg && getSize("lg", lg)}
	${xl && getSize("xl", xl)}
`;

export default styles;
