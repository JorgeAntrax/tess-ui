import { $, connect, useStyles } from "tess-core";
import styles from "./styles";
import props from "./props";
import { Host } from "layout";

function uiContent(props) {
  const { css } = useStyles(styles, { ...props }, [
    props.xs,
    props.sm,
    props.md,
    props.lg,
    props.xl,
    props.wrapper,
    props.dev,
  ]);


  return $(
    <Host stylesheet={css}>
      {props.wrapper ? (
        <div>
          <slot></slot>
        </div>
      ) : (
        <slot></slot>
      )}
    </Host>
  );
}

export default connect(uiContent)({ props });
