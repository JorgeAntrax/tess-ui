import { $, connect, useStyles } from "tess-core";
import styles from "./styles";
import props from "./props";
import { Host } from "layout";

function uiGrid(attrs) {
  const { css } = useStyles(styles, { ...attrs }, [...props]);

  return $(
    <Host stylesheet={css}>
		<slot></slot>
    </Host>
  );
}

export default connect(uiGrid)({ props });
