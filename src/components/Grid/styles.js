import { css } from "tess-core";

const styles = ( { cols, inline, autofit, autofill, autoflow, space, colSpace, rowSpace } ) => css`
  :host {
	width: var(--ui-grid-w, ${ inline ? 'auto' : '100%' });
	display: ${ inline ? 'inline-grid' : 'grid' };
	box-sizing: border-box;
	position: relative;

	grid-template-columns: repeat(${ autofit ? 'auto-fit' :
		autofill ? 'auto-fill' :
			'var(--ui-grid-auto-cols, '+(cols || 20)+')'
	}, minmax(5%, 1fr));

	${ colSpace ? 'grid-column-gap: var(--ui-grid-col-gap, ' + ( colSpace || 0 ) + 'px);' : '' }
	${ rowSpace ? 'grid-row-gap: var(--ui-grid-row-gap, ' + ( rowSpace || 0 ) + 'px);' : '' }
	${ space ? 'grid-gap: var(--ui-grid-gap, ' + ( space || 0 ) + 'px);' : '' }

	${ autoflow ? 'grid-auto-flow: var(--ui-grid-autoflow, ' + ( autoflow || 'row' ) + ');' : '' }
  }

  div {
	  background-color: red;
	  color: white;
  }

  :host-context(ui-navbar) {
	  flex: 1;
  }
`;

export default styles;
