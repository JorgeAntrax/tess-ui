function Close({ w, fill, color }) {
  return (
    <svg
      width={w || 24}
      height={w || 24}
      fill={fill || "none"}
      viewBox="0 0 24 24"
    >
      <path
        d="M18.75 5.25L5.25 18.75"
        stroke={color || "black"}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M18.75 18.75L5.25 5.25"
        stroke={color || "black"}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default Close