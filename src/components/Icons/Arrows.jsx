function ArrowLeft({ color, stroke }) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="32"
      height="32"
      fill="none"
      viewBox="0 0 32 32"
    >
      <path
        stroke={color || "black"}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={stroke || 1}
        d="M20 26L10 16 20 6"
      ></path>
    </svg>
  );
}

function ArrowRight({ color, stroke }) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="32"
      height="32"
      fill="none"
      viewBox="0 0 32 32"
    >
      <path
        stroke={color || "black"}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={stroke || 1}
        d="M12 6l10 10-10 10"
      ></path>
    </svg>
  );
}

export { ArrowRight, ArrowLeft }
