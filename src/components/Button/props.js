// Observable properties
export default [
	"primary",
	"secondary",
	"variant",
	"type",
	"rounded",
	"loading",
	"icon",
	"href",
	"to"
]