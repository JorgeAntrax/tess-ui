import { $, connect, useStyles } from 'tess-core'
import styles from './styles'
import props from './props'
import { Host } from 'layout'

function uiButton(attrs) {
	const { css } = useStyles(styles, {...attrs}, [...props])

	return $(
		<Host stylesheet={css}>
			{attrs.loading && (
				<>
					<span id="loading"></span>
					<span id="loading-text">
						<slot></slot>
					</span>
				</>
			)}
			{!attrs.loading && <slot></slot>}
		</Host>
	)
}

export default connect(uiButton)({ props })