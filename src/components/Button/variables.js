export default {
	base: {
		primary: "--ui--primary",
		secondary: "--ui--secondary",
		success: "--ui--success",
		warning: "--ui--warning",
		danger: "--ui--danger",
		dark: "--ui--dark",
		color: "--ui--color"
	}
}