import { css, getCssProperty } from 'tess-core'
import { parseToRGB, darken } from 'styles/theme'

const styles = ({ rounded, icon, primary, secondary, variant, loading }) => {
	let style = ''
	const $bg = '#efefef'
	const $text = '#444444'

	const base = getCssProperty('font-base')
	const radius = rounded ? '40px' : '2px'

	const background = primary
		? getCssProperty('primary') || $bg
		: secondary
		? getCssProperty('secondary') || $bg
		: $bg

	const color = primary
		? getCssProperty('primary-color') || $text
		: secondary
		? getCssProperty('secondary-color') || $text
		: $text

	switch (variant) {
		case 'outline':
			style = css`
				:host {
					background-color: transparent;
					border-color: ${background};
					color: var(--color, ${background});
				}

				:host(:hover) {
					border-color: ${background};
					background-color: ${background};
					color: var(--color, ${color});
				}

				${loading &&
				css`
					:host #loading {
						border: 1px solid ${background};
						border-top-color: transparent;
						border-right-color: transparent;
						border-bottom-color: ${parseToRGB(background, 0.4)};
					}
				`}
			`
			break
		case 'link':
			style = css`
				:host {
					border-color: transparent;
					background-color: transparent;
					color: ${background};
				}

				:host(:hover) {
					background-color: ${parseToRGB(background, 0.2)};
				}

				${loading &&
				css`
					:host #loading {
						border: 1px solid ${background};
						border-top-color: transparent;
						border-right-color: transparent;
						border-bottom-color: ${parseToRGB(background, 0.4)};
					}
				`}
			`
			break

		case 'fill':
			style = css`
				:host {
					border-color: transparent;
					background-color: ${parseToRGB(background, 0.2)};
					color: ${background};
				}

				:host(:hover) {
					border-color: ${background};
					background-color: transparent;
				}

				${loading &&
				css`
					:host #loading {
						border: 1px solid ${background};
						border-top-color: transparent;
						border-right-color: transparent;
						border-bottom-color: ${parseToRGB(background, 0.4)};
					}
				`}
			`
			break

		default:
			style = css`
				:host {
					background-color: ${background};
					color: ${color};
				}

				:host(:hover) {
					background-color: ${darken(background, 20)};
				}

				${loading &&
				`
					:host #loading {
						border: 1px solid ${color};
						border-top-color: transparent;
						border-right-color: transparent;
						border-bottom-color: ${parseToRGB(color, 0.4)};
					}
				`}
			`
			break
	}

	return css`
		:host {
			line-height: 1.2;
			display: inline-grid;
			grid-auto-flow: column;
			grid-auto-columns: min-content max-content auto;
			grid-gap: calc(${base} / 2);
			align-items: center;
			align-content: center;
			justify-content: center;
			line-height: inherit;
			font-family: inherit;
			font-size: inherit;
			transition: all 100ms linear;
			padding: calc(${base} / 2) ${base};
			border-radius: ${radius};
			border: 1px solid transparent;
			overflow: hidden;
			cursor: pointer;
		}

		:host(:hover) {
			transform: var(--hover-effect, translateY(-5px));
		}

		:host * {
			pointer-events: none;
			user-select: none;
		}

		${style && style}

		${loading &&
		css`
			:host {
				pointer-events: none;
			}

			:host #loading-text {
				font: inherit;
				opacity: 0.5;
				z-index: 1;
			}
			:host #loading {
				z-index: 2;
				position: absolute;
				top: 50%;
				left: 50%;
				display: block;
				width: calc(${base} + 8px);
				height: calc(${base} + 8px);
				min-width: calc(${base} + 8px);
				min-height: calc(${base} + 8px);
				border-radius: 4em;

				transform: translate(-50%, -50%) rotate(0);
				animation-name: loading;
				animation-duration: var(--timer, 600ms);
				animation-timing-function: var(--type, linear);
				animation-iteration-count: infinite;
				animation-delay: var(--delay, 0ms);
			}

			@keyframes loading {
				from {
					transform: translate(-50%, -50%) rotate(0);
				}
				to {
					transform: translate(-50%, -50%) rotate(360deg);
				}
			}
		`}

		${icon &&
		css`
			:host {
				width: var(--icon, 40px);
				height: var(--icon, 40px);
				min-width: var(--icon, 40px);
				min-height: var(--icon, 40px);
				padding: 5px;
			}
		`}
	`
}

export default styles