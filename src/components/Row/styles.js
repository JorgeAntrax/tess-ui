import { css, getCssProperty } from "tess-core";

const mapToProps = {
  center: "center",
  start: "flex-start",
  end: "flex-end",
  around: "space-around",
  between: "space-between",
};

const devStyles = `
	box-shadow: inset 0 0 0 1px rgba(0, 0, 230, 0.5);
	padding: 2px;
	border-radius: 4px;
`;

const Justify = (key, value) => css`
  @media screen and (min-width: ${getCssProperty("media-" + key)}) {
    :host {
      justify-content: ${value};
    }
  }
`;

const Align = (key, value) => css`
  @media screen and (min-width: ${getCssProperty("media-" + key)}) {
    :host {
      align-items: ${value};
      align-content: ${value};
    }
  }
`;

const Center = key => css`
  @media screen and (min-width: ${getCssProperty("media-" + key)}) {
    :host {
      align-items: center;
      align-content: center;
      justify-content: center;
    }
  }
`;

const styles = ({
  start,
  center,
  centered,
  end,
  top,
  middle,
  bottom,
  between,
  around,
  inline,
  nowrap,
  dev,
}) => css`
	:host {
		width: 100%;
		height: auto;
		max-width: 100%;
		flex-wrap: var(--ui-row-wrap, ${nowrap ? 'nowrap' : 'wrap'});
		display: var(--ui-row-display , ${inline ? "inline-flex" : "flex"});
		position: var(--ui-position, relative);
		${dev && devStyles}
	}


	:host[autofill] > ui-column {
		flex: 1;
	}

	${start && Justify(start, mapToProps.start)}
	${end && Justify(end, mapToProps.end)}
	${center && Justify(center, mapToProps.center)}

	${top && Align(top, mapToProps.start)}
	${bottom && Align(bottom, mapToProps.end)}
	${middle && Align(middle, mapToProps.center)}

	${around && Justify(around, mapToProps.around)}
	${between && Justify(between, mapToProps.between)}
	
	${centered && Center(centered)}
`;

export default styles;
