import { $, connect, useStyles } from "tess-core";
import styles from "./styles";
import props from "./props";
import { Host } from "layout";

function uiRow(props) {
  const { css } = useStyles(styles, { ...props }, [
    props.start,
    props.center,
    props.centered,
    props.end,
    props.top,
    props.middle,
    props.bottom,
    props.between,
    props.around,
    props.inline,
    props.nowrap,
    props.dev
  ]);

  return $(
    <Host stylesheet={css}>
      <slot></slot>
    </Host>
  );
}

export default connect(uiRow)({ props });
