export default [
  "start",
  "center",
  "centered",
  "end",
  "top",
  "middle",
  "bottom",
  "between",
  "around",
  "inline",
  "nowrap",
  "dev",
];
