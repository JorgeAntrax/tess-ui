export default [
  "xs",
  "sm",
  "md",
  "lg",
  "xl",
  "inline",
  "self",
  "offset",
  "dev",
];
