import { css, getCssProperty } from "tess-core";

const mapToProps = {
  center: "center",
  start: "flex-start",
  end: "flex-end",
  around: "space-around",
  between: "space-between",
};

const getSize = (key, value) => {
  const media = `media-${key}`;

  if (!value.length) return "";

  if (value.indexOf("px") !== -1 || value.indexOf("%") !== -1) {
    return css`
      @media screen and (min-width: ${getCssProperty(media)}) {
        :host {
          width: ${value};
        }
      }
    `;
  }

  if (isNaN(value)) return "100%";
  return css`
    @media screen and (min-width: ${getCssProperty(media)}) {
      :host {
        width: ${+value * 5}%;
      }
    }
  `;
};

const devStyles = `
	background-color: rgba(255,0,0,0.2); 
	box-shadow: inset 0 0 0 1px white;
	padding: 2px;
	border-radius: 4px;
`;

const selfStyle = `
	align-self: ${mapToProps[self]};
`;

const styles = ({ xs, sm, md, lg, xl, self, inline, offset, dev }) => css`
	:host {
		position: var(--ui-col-pos, relative);
		display: var(--ui-col-display, ${inline ? "inline-block" : "block"});
		padding: var(--ui-col-gap, 0);
		margin-left: var(
			--ui-col-offset,
			${offset ? offset + (offset > 100 ? "px" : "%") : "0"}
		);

		height: auto;
		${self && selfStyle}
	}
	
	:host-context(ui-row[dev]) {
		${devStyles}
	}

	${xs && getSize("xs", xs)}
	${sm && getSize("sm", sm)}
	${md && getSize("md", md)}
	${lg && getSize("lg", lg)}
	${xl && getSize("xl", xl)}
`;

export default styles;
