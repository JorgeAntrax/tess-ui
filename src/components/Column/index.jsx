import { $, connect, useStyles } from "tess-core";
import styles from "./styles";
import props from "./props";
import { Host } from "layout";

function uiColumn(props) {
  const { css } = useStyles(styles, { ...props }, [
    props.xs,
    props.sm,
    props.md,
    props.lg,
    props.xl,
    props.self,
    props.inline,
    props.offset,
	props.dev
  ]);

  return $(
    <Host stylesheet={css}>
      <slot></slot>
    </Host>
  );
}

export default connect(uiColumn)({ props });
