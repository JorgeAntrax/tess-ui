import { css } from "tess-core";

const styles = (props) => css`
  :host {
    display: block;
    height: var(--ui-nav-height, 60px);
    width: 100%;
    position: var(--ui-nav-position, sticky);
    background-color: var(--ui-nav-bg, white);
    z-index: var(--ui-nav-index, 2);
  }

  :host nav {
    width: 100%;
    height: 100%;
    position: relative;
    display: flex;
    flex-wrap: nowrap;
    max-width: var(--ui-nav-maxw, 90%);
    justify-content: var(--ui-nav-justify, space-between);
    align-items: var(--ui-nav-align, center);
    align-content: var(--ui-nav-align-c, center);
    margin-left: auto;
    margin-right: auto;
  }
`;

export default styles;
