import { $, connect, useStyles } from "tess-core";
import styles from "./styles";
import { Host } from "layout";

function uiNavbar(props) {
  const { css } = useStyles(styles, { ...props }, []);

  return $(
    <Host stylesheet={css}>
      <nav>
        <slot></slot>
      </nav>
    </Host>
  );
}

export default connect(uiNavbar)({ props: [] });
