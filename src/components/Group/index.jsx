import { $, connect, useStyles } from "tess-core";
import styles from "./styles";
import { Host } from "layout";

function uiGroup(props) {
  const { css } = useStyles(styles, { ...props }, []);

  return $(
    <Host stylesheet={css}>
		<slot></slot>
    </Host>
  );
}

export default connect(uiGroup)({ props: ['inline'] });
