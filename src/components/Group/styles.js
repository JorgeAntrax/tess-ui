import { css } from "tess-core";

const styles = ({inline}) => css`
  :host {
    display: ${inline ? 'inline-flex' : 'flex'};
    width: var(--ui-group-width, ${inline ? 'auto' : '100%'});
  }
`;

export default styles;