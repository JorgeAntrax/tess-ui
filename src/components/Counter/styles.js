import { css } from 'tess-core';

const flex = `
	display: inline-flex;
	align-items: center;
	justify-content: center;
`

const styles = ( { size } ) => css`
	:host {
		${ flex };
		position: relative;
		overflow: hidden !important;
		border: 1px solid rgba(0, 0, 0, 0.2);
		border-radius: var(--ui-control-radius, 15px);

	}

	:host(:hover) {
		background-color: white;
		border-color: transparent;
		border-radius: var(--ui-control-radius, 15px);
		box-shadow: 0 5px 10px -5px rgba(0,0,0,0.2);
	}

	button img {
		position: relative;
		object-fit: contain;
		width: 100%;
		height: 100%;
	}

	button {
		${ flex };
		padding: 0.5rem;
		width: var(--ui-control-width, ${ size || 40 }px);
		height: var(--ui-control-height, ${ size || 40 }px);
		min-width: var(--ui-control-width, ${ size || 40 }px);
		min-height: var(--ui-control-height, ${ size || 40 }px);
		border: var(--ui-control-button-border, none);
		background-color: var(--ui-control-button-bg, transparent);
		border-radius: var(--ui-control-radius, 15px);
		transition: all .2s linear;
		cursor: pointer;
		overflow: hidden;
	}

	button:hover {
		background-color: var(--ui-control-button-bg--hover, rgba(0,0,0,0.05));
		border-radius: var(--ui-control-radius, 15px);
	}

	button:focus,
	button:active {
		outline: 0;
	}

	button * {
		pointer-events: none !important;
	}

	span {
		${ flex };
		min-width: 30px;
		padding-left: 0.5rem;
		padding-right: 0.5rem;
		line-height: 1;
		user-select: none;
		color: var(--ui-control-color, #444);
	}
`

export default styles