import {
  $,
  connect,
  useState,
  useEffect,
  useStyles,
  useEvent,
} from "tess-core";

import styles from "./styles";
import props from "./props";
import { Host } from "layout";

import { ArrowLeft, ArrowRight } from "components";

function uiCounter(props) {
  const { css } = useStyles(styles, { ...props }, [props.color, props.size]);

  const [initialEvent, setInitialEvent] = useState(false);
  const [_iconless, setIconless] = useState(null);
  const [counter, setCounter] = useState(1);
  const [_step, setStep] = useState(1);
  const [_min, setMin] = useState(1);
  const [_max, setMax] = useState(10);

  useEffect(() => {
    setTimeout(() => {
      const { min, max, step, value, iconless } = props;

      +value && setCounter(+value);
      +min && setMin(+min);
      +max && setMax(+max);
      +step && setStep(+step);

      iconless && setIconless(iconless);
      !iconless && setIconless(false);
      setInitialEvent(true);
    }, 500);
  }, []);

  useEffect(() => {
    +counter && (props.value = counter);
    +_min && (props.min = _min);
    +_max && (props.max = _max);
    +_step && (props.step = _step);

    initialEvent &&
      useEvent(this, "change", {
        step: _step,
        min: _min,
        max: _max,
        value: counter,
      }).emit();
  }, [counter, _step, _min, _max]);

  useEffect(() => {
    _min > counter && setCounter(_min);
  }, [_min]);

  useEffect(() => {
    _step > _min && setMin(_step);
    _step > counter && setCounter(_step);
  }, [_step]);

  useEffect(() => {
    if (+props.value > _max) {
      console.error("value property exceeds defined maximum");

      return;
    }
    if (+props.value < _min) {
      console.error("the value property is less than the defined minimum");

      return;
    }

    +props.value && setCounter(+props.value);
  }, [props.value]);

  useEffect(() => {
    +props.step && setStep(+props.step);
  }, [props.step]);

  useEffect(() => {
    +props.min && setMin(+props.min);
  }, [props.min]);

  useEffect(() => {
    +props.max && setMax(+props.max);
  }, [props.max]);

  const handleNext = () => {
    const value = counter > _min ? counter - _step : _min;
    setCounter(value);
  };

  const handleBack = () => {
    const value = counter < _max ? counter + _step : _max;
    setCounter(value);
  };

  return $(
    <Host stylesheet={css}>
      <button id="prev" onClick={() => handleNext()}>
        {_iconless !== null && _iconless === false ? (
          <ArrowLeft color={props.color} />
        ) : (
          <slot name="prev"></slot>
        )}
      </button>
      <span>{counter}</span>
      <button onClick={() => handleBack()}>
        {_iconless !== null && _iconless === false ? (
          <ArrowRight color={props.color} />
        ) : (
          <slot name="next"></slot>
        )}
      </button>
    </Host>
  );
}

export default connect(uiCounter)({ props });
