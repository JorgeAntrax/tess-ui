import { css, getCssProperty, parseToRGB } from "tess-core";

const getThemeColor = ({ _scheduler, ...props }, alpha) => {
  for (let key in props) {
    if (props[key]) {
      const color = getCssProperty(key).replace(/#/g, "");
      if (color.length > 0) {
        return parseToRGB(
          color,
          key === "warning" && alpha !== 1 ? 0.4 : alpha
        );
      }
    }
  }

  return `rgba(0, 0, 0, ${alpha})`;
};

const getSize = (key, value) => {
  if (!value.length) return "";
  if (value.indexOf("px") !== -1 || value.indexOf("%") !== -1) {
    return css`
      @media screen and (min-width: ${getCssProperty("media-" + key)}) {
        :host {
          width: ${value};
        }
      }
    `;
  }

  if (isNaN(value)) return "";
  return css`
    @media screen and (min-width: ${getCssProperty("media-" + key)}) {
      :host {
        width: ${+value * 5}%;
      }
    }
  `;
};

const styles = ({ xs, sm, md, lg, xl, ...props }) => css`
	:host {
		width: 100%;
		display: inline-flex;
		align-items: start;
		align-content: start;
		justify-content: space-between;
		position: relative;
		border-radius: 4px 10px 10px 4px;
		box-shadow: 0 5px 10px -5px rgba(0, 0, 0, 0.2);
		overflow: hidden;
		padding: 1.25rem 1rem 1.25rem 1.25rem;
		border-left: 4px solid var(--ui-notify-color, ${getThemeColor(props, 1)});
		font-size: var(--ui-fontSize, 16px);
		transition: all 200ms linear;
		color: #444;
	}

	:host #close {
		border-radius: 5em;
		width: 18px;
		height: 18px;
		min-width: 18px;
		min-height: 18px;
		display: inline-flex;
		align-items: center;
		align-content: center;
		justify-content: center;
		padding: 3px;
		opacity: 0.7;
		transition: all 200ms linear;
		cursor: pointer;
		margin-left: 1.5rem;
	}
	
	:host(:hover) {
		box-shadow: 0 15px 35px -5px rgba(0, 0, 0, 0.1);
	}

	:host(:hover) #close:hover {
		opacity: 1;
		background-color: ${getThemeColor(props, 0.1)};
	}

	:host(:hover) #close:active {
		background-color: ${getThemeColor(props, 0.2)};
	}

	:host #close img {
		width: 100%;
		height: 100%;
		object-fit: contain;
	}

	${xs && getSize("xs", xs)}
	${sm && getSize("sm", sm)}
	${md && getSize("md", md)}
	${lg && getSize("lg", lg)}
	${xl && getSize("xl", xl)}
`;

export default styles;
