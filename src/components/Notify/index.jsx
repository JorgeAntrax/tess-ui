import { $, connect, useStyles, useEvent } from "tess-core";
import styles from "./styles";
import props from "./props";
import { Host } from "layout";
import { Close } from "components";

function uiNotify(props) {
  const { css } = useStyles(styles, { ...props }, [
    props.unclose,
    props.primary,
    props.secondary,
    props.success,
    props.warning,
    props.danger,
    props.dark,
    props.color,
  ]);

  const onClose = () => {
    useEvent(this, "close", {
      closeable: true,
    }).emit();
  };

  return $(
    <Host stylesheet={css}>
      <slot></slot>
      {!props.unclose && (
        <span id="close" onClick={() => onClose()}>
          <Close w={24} />
        </span>
      )}
    </Host>
  );
}

export default connect(uiNotify)({ props });