import { css } from "tess-core";
/**
 * @param {children} param0
 * @type {JSXObject}
 * @description {The web component content}
 *
 * @param {stylesheet} param1
 * @type {object}
 * @description {Have two proprties: styles and props for styles rendering}
 * @returns
 */

const $host = css`
  :host {
    all: initial;
    position: relative;
    font: inherit;
    transition: opacity 200ms linear;
    opacity: 0;
  }

  :host(:defined) {
    opacity: 1;
  }

  :host([hidden]) {
    display: none;
  }

  :host([disabled]) * {
    pointer-events: none;
  }

  :host([disabled]) {
    cursor: not-allowed;
    opacity: 0.6;
  }

  :host([hidden]),
  :host([hidden]) *,
  :host([disabled]),
  :host([disabled]) *,
  :host([freeze]),
  :host([freeze]) * {
    pointer-events: none;
  }

  :host *,
  :host *:after,
  :host *:before {
    box-sizing: border-box;
  }
`;

const Host = ({ children, stylesheet = [] }) => {
  const $css = Array.isArray(stylesheet) ? stylesheet.join('') : stylesheet;
  return (
    <>
      <style>{$host}{$css}</style>
      {children && <>{children}</>}
    </>
  );
};

export default Host;
