import { css } from 'tess-core'
export default () => css`
	:host-context(body) {
		overflow-x: hidden;
	}
	:host {
		display: block;
		width: 100%;
		max-width: 100%;
		padding: 0.5rem;
		box-sizing: border-box;
	}
`