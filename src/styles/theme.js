const theme = {
	colors: {
		primary: '#0089FF',
		secondary: '#233D4D',
		success: '#26E28A',
		warning: '#FFD639',
		danger: '#EE2E31',
		dark: '#232525',
		light: '#f1f1f1',
		white: '#FFFFFF',
	},
	shadows: {
		primary: '#0089FF',
		secondary: '#233D4D',
		success: '#26E28A',
		warning: '#FFD639',
		danger: '#EE2E31',
		light: '#f1f1f1',
		white: '#FFFFFF',
		dark: 'rgba(0, 0, 0,0.3)',
	},
}

const breakpoints = {
	xs: '320px',
	sm: '520px',
	md: '768px',
	lg: '980px',
	xl: '1280px',
}

const customCSSProps = {
	$padding: '--ui-padding',
	$margin: '--ui-margin',
	$radius: '--ui-radius',
	$line: '--ui-line',
	$width: '--ui-width',
	$height: '--ui-height',
	$family: '--ui-ft',
	$size: '--ui-fs',
	$weight: '--ui-fw',
}

const getTheme = (data) => {
	const { colors, shadows } = theme
	let style = ''
	for (let prop in data) {
		if (data[prop]) {
			style = prop
		}
	}

	const text = ['warning', 'white', 'light'].includes(style)
		? colors.dark
		: colors.white

	const shadow = ['white', 'light'].includes(style)
		? shadows.dark
		: shadows[style]

	return { style, text, shadow }
}

const getColorRGB = function (b) {
	let a
	if (b && b.constructor == Array && b.length == 3) return b
	if (
		(a =
			/rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/.exec(
				b
			))
	)
		return [parseInt(a[1]), parseInt(a[2]), parseInt(a[3])]
	if (
		(a =
			/rgb\(\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*\)/.exec(
				b
			))
	)
		return [
			parseFloat(a[1]) * 2.55,
			parseFloat(a[2]) * 2.55,
			parseFloat(a[3]) * 2.55,
		]
	if ((a = /#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})/.exec(b)))
		return [parseInt(a[1], 16), parseInt(a[2], 16), parseInt(a[3], 16)]
	if ((a = /#([a-fA-F0-9])([a-fA-F0-9])([a-fA-F0-9])/.exec(b)))
		return [
			parseInt(a[1] + a[1], 16),
			parseInt(a[2] + a[2], 16),
			parseInt(a[3] + a[3], 16),
		]
	return typeof colors != 'undefined'
		? colors[jQuery.trim(b).toLowerCase()]
		: null
}

const isDarkenColor = function (color) {
	var rgb = getColorRGB(color)
	if (!rgb) return null
	return 0.2126 * rgb[0] + 0.7152 * rgb[1] + 0.0722 * rgb[2] > 180
}

const toHex = (color) => {
	let hexadecimal = color.toString(16)
	return hexadecimal.length == 1 ? '0' + hexadecimal : hexadecimal
}

const parseToRGB = (hex, alpha = 1) => {
	const [r, g, b] = hex.match(/\w\w/g).map((x) => parseInt(x, 16))
	return `rgba(${r},${g},${b},${alpha})`
}

const parseToHex = (r, g, b) => {
	return '#' + toHex(r) + toHex(g) + toHex(b)
}

const mapToProp = (prop) => {
	prop = prop.split(',')
	return prop.join('px ') + 'px'.trim()
}

const darken = (color, amount) => {
	const [r, g, b] = color.match(/\w\w/g).map((x) => parseInt(x, 16))

	let rr = r - amount
	let gg = g - amount
	let bb = b - amount

	rr = rr < 0 ? 0 : rr
	gg = gg < 0 ? 0 : gg
	bb = bb < 0 ? 0 : bb

	return parseToHex(rr, gg, bb)
}

const lighten = (color, amount) => {
	const [r, g, b] = color.match(/\w\w/g).map((x) => parseInt(x, 16))

	let rr = r + amount
	let gg = g + amount
	let bb = b + amount

	rr = rr > 255 ? 255 : rr
	gg = gg > 255 ? 255 : gg
	bb = bb > 255 ? 255 : bb

	return parseToHex(rr, gg, bb)
}

export {
	getTheme,
	customCSSProps,
	isDarkenColor,
	parseToRGB,
	mapToProp,
	breakpoints,
	lighten,
	darken,
}
export default theme
