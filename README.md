# Tess UI Library JSX Web Components

git clone https://gitlab.com/JorgeAntrax/tess-ui.git

Tess is a UI library for creating web components using the full potential of react hooks thanks to the haunted js and JSX Syntax.


### Getting started

`npm install tess-ui`

After installing the Node JS package

run the following

`npm install`

all dependencies will be installed including the `tess-core` package

now run the webpack script

`npm run build`

there are some web components inside index.html, open it in the browser