const path = require('path')

module.exports = {
	mode: 'development',
	watchOptions: {
		ignored: '**/node_modules',
		poll: 1000,
		aggregateTimeout: 600,
	},
	resolve: {
		alias: {
			src: path.resolve(__dirname, 'src'),
			components: path.resolve(__dirname, 'src/components'),
			layout: path.resolve(__dirname, 'src/layout'),
			styles: path.resolve(__dirname, 'src/styles'),
		},
		extensions: ['.js', '.jsx'],
	},
	entry: {
		index: './index.js',
		button: './src/components/Button/index.js',
	},
	output: {
		path: path.join(__dirname, 'public'),
		filename: '[name].js',
	},
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
				},
			},
			{
				test: /\.svg$/,
				use: [
					{
						loader: 'svg-url-loader',
						options: {
							limit: 10000,
						},
					},
				],
			},
			{
				test: /\.(ts|tsx)/,
				exclude: /node_modules/,
				use: ['ts-loader'],
			},
		],
	},
}
